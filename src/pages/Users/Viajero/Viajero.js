import React, { useState, useEffect } from "react";
/* INCONOS */
import VisibilityIcon from "@material-ui/icons/Visibility";
import { Drawer } from "@material-ui/core";
import DeleteIcon from '@material-ui/icons/Delete';
import CreateIcon from '@material-ui/icons/Create';
import { makeStyles } from "@material-ui/core/styles";
/* COMPONENTES */
import { AgregarViajero } from "../../../components/Drawers/AgregarViajero";
import { EditarViajero } from "../../../components/Drawers/EditarViajero";
import { EliminarViajero } from "../../../components/Modales/EliminarViajero/EliminarViajero";
import { Paginator } from "../../../components/Paginator/Paginator";
/* SERVICIOS */
import { ViajeroService } from '../../../service/ViajeroService';
/* ESTILOS */
import "./Viajero.scss";
const useStyles = makeStyles({
  paper: {
    "@media only screen and (min-width: 370px) and (max-width: 530px)": {
      width: 580,
    },
    "@media only screen and (max-width: 370px)":{
      width: 390,
    }  
  },
});


export const Viajero = () => {
  const classes = useStyles();
  const [page, setPage] = useState(0); // controlar la paginacin de la tabla
  const [filter, setFilter] = useState(""); // filtrado de datos
  const [drawerAgregar, setDrawerAgregar] = useState(false); // abrir y cerrar Drawer de agregar
  const [drawerEditar, setDrawerEditar] = useState(false); // abrir y cerrar Drawer de editar
  const [modalIsOpen, setIsOpen] = React.useState(false); // abrir y cerrar modal de eliminar
  const [viajeros, setViajeros] = useState(null); // data de la api rest
  const [datos, setDatos] = React.useState({ // auxiliar para los datos de agregar viajero
    nombre: false,
    cedula: false,
    fecha: false,
    telefono: false,
    viaje: false,
    id:false
  });
  const [dataEditar, setDataEditar] = React.useState(false); // auxiliar para los datos de editar viajero

 
  useEffect(() => {
    const viajeroService = new ViajeroService();
    /*** se carga la data de la api */
    viajeroService.getViajeros().then(data =>{
      setViajeros(data) 
    });

  }, []);

  /* Con esta funcion agrupamos la data en paginas */
  const getDataPerPage = () => {
    let dataAux = viajeros ? viajeros : data;
    if (filter !== "") {
      dataAux = dataAux.filter(
        (row) =>
          JSON.stringify(row.id).includes(filter) ||
          row.cedula.toLowerCase().includes(filter) ||
          row.nombre.toLowerCase().includes(filter) ||
          row.telefono.toLowerCase().includes(filter) ||
          row.viaje.toLowerCase().includes(filter) ||
          row.fecha.toLowerCase().includes(filter)
      );
    }

    let agroupedData = [];

    do {
      agroupedData = [...agroupedData, [...dataAux.slice(0, 5)]];
      dataAux = dataAux.slice(5, dataAux.length);
    } while (dataAux.length > 0);

    return agroupedData;
  };


  const prevPage = () => {
    if (page > 0) {
      setPage(page - 1);
    }
  };

  const nextPage = () => {
    if (page < getDataPerPage().length - 1) {
      setPage(page + 1);
    }
  };

  const handleChange = (e) => {
   // setFilter(e.target.value.toLowerCase());
  };
      /*** handleChange del formulario drawer agregar viajero */
  const handleChangeAgregar= (event) => {
    setDatos({ ...datos, [event.target.name]: event.target.value });
  };
      /*** handleChange del formulario drawer editar viajero */
  const handleChangeEditar= (event) => {
    setDataEditar({ ...dataEditar, [event.target.name]: event.target.value });
  };
  /*** funcion para registrar un viajero */
  const agregarViajero = async () => {
    const viajeroService = new ViajeroService();
    viajeroService.addViajero(datos).then(data =>{
      viajeroService.getViajeros().then(data =>{setViajeros(data)});
      setDrawerAgregar(false);
    }); 
  }
    /*** funcion para actualizar un viajero */
  const actualizarViajero = async () => {
    const viajeroService = new ViajeroService();
    viajeroService.updateViajero(dataEditar).then(data =>{
      viajeroService.getViajeros().then(data =>{setViajeros(data)});
      setDrawerEditar(false);
    }); 
  }
    /*** funcion para eliminar un viajero */
  const eliminarViajero = async () => {
    const viajeroService = new ViajeroService();
    viajeroService.eliminarViajero(datos.id).then(data =>{
      viajeroService.getViajeros().then(data =>{setViajeros(data)});
      setIsOpen(false);
    }); 
  }
    /*** funcion para button de editar viajero */
  const DrawerEditar = (row) => {
    setDataEditar(row) // se guarda los datos del item en una variable auxiliar
    setDrawerEditar(true) // se abre el drawer de editar viajero
  }

  /**
   * Funcion para abrir o cerrar el sidenav
   * @param {*} open true para abrir, false para cerrar
   */
  const toggleDrawer = (open) => (event) => {
    if (event.type === "keydown" && (event.key === "Tab" || event.key === "Shift")) {
      return;
    }

    setDrawerAgregar(open);
    setDrawerEditar(open);
  };
  /** abrir el modal  */
  function openModal(id) {
    setIsOpen(true);
    setDatos({ ...datos, id });
  }
  /** cerrar el modal  */
  function closeModal() {
    setIsOpen(false);
  }

  return (
    <>
      <div className="flex pt-5 pb-8 justify-between gap-20">
        <div className="flex w-full">
          <input
            placeholder="Busqueda de Ofertas"
            onChange={(e) => handleChange(e)}
            className="custom-input mr-10 w-full i-max-w-1"
          />

          <button onClick={() => setDrawerAgregar(true)} className="btn btn-1 i-w-1" variant="contained">
            Agregar
          </button>
        </div>
      </div>
      {data ? (
      <table className="table">
        <tr>
          <th className="text-xl">Nombre</th>
          <th className="text-xl">Cedula</th>
          <th className="text-xl">Teléfono</th>
          <th className="text-xl">fecha Nacimiento</th>
          <th className="text-xl">Vuelo</th>
          <th className="text-xl">Acciones</th>
        </tr>
        {getDataPerPage()[page].map((row,key) => (
          <tr key={key}>
            <td className="row-item text-base">{row.nombre}</td>
            <td className="row-item text-base">{row.cedula}</td>
            <td className="row-item text-base">{row.telefono}</td>
            <td className="row-item text-base">{row.fecha}</td>
            <td className="row-item text-base"><VisibilityIcon /></td>
            <td>
              <button onClick={() => DrawerEditar(row)}>
                <CreateIcon />
              </button>
              <button onClick={() => openModal(row.id)}>
                <DeleteIcon />
              </button>
            </td>
          </tr>
        ))}
      </table>
      ) : (
        <p>Cargando resultados...</p>
      )} 

      <Paginator page={page} setPage={setPage} prevPage={prevPage} nextPage={nextPage} pageArray={getDataPerPage()} />

      {/*Area de Drawers*/}

      <Drawer anchor="right" classes={{ paper: classes.paper }} open={drawerAgregar} onClose={toggleDrawer(false)}>
        <AgregarViajero toggleDrawer={toggleDrawer} agregarViajero={agregarViajero} handleChangeAgregar={handleChangeAgregar} />
      </Drawer>

      <Drawer anchor="right" classes={{ paper: classes.paper }} open={drawerEditar} onClose={toggleDrawer(false)}>
        <EditarViajero dataEditar={dataEditar} actualizarViajero={actualizarViajero} toggleDrawer={toggleDrawer} handleChangeAgregar={handleChangeEditar} />
      </Drawer>

      {/*Area de Modales*/}
      <EliminarViajero eliminarViajero={eliminarViajero} modalIsOpen={modalIsOpen} closeModal={closeModal} />
    </>
  );
};

const data = [
  {
    id: 1,
    nombre: "Gas natural S.A.S",
    cedula: "gas_natural@gmail.com",
    telefono: "3016578474",
    fecha: "3016578474",
    viaje: "Proveedor",
  },
]
