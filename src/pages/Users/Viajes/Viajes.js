import React, { useState, useEffect } from "react";
/* ICONOS Y DRAWER*/
import DeleteIcon from '@material-ui/icons/Delete';
import CreateIcon from '@material-ui/icons/Create';
import { Drawer } from "@material-ui/core";
/* COMPONENTES */
import { Paginator } from "../../../components/Paginator/Paginator";
import { AgregarViaje } from "../../../components/Drawers/AgregarViaje";
import { EditarViaje } from "../../../components/Drawers/EditarViaje";
import { EliminarViaje } from "../../../components/Modales/EliminarViaje/EliminarViaje";
/* ESTILOS */ 
import "./Viajes.scss";
/** SERVICIOS */
import { ViajeService } from '../../../service/ViajeService';


export const Viajes = () => {
  const [page, setPage] = useState(0); // controlar la paginacion de la tabla
  const [filter, setFilter] = useState(""); // filtrado de datos
  const [drawerAgregar, setDrawerAgregar] = useState(false); // abrir y cerrar Drawer de agregar
  const [drawerEditar, setDrawerEditar] = useState(false); // abrir y cerrar Drawer de editar
  const [modalIsOpen, setIsOpen] = React.useState(false); // abrir y cerrar modal de eliminar
  const [viajes, setViajes] = useState(null); // data de la api rest
  const [datos, setDatos] = React.useState({ // auxiliar para los datos de agregar viaje
    codigo: false,
    numero_plazas: false,
    destino: false,
    lugar: false,
    precio: false,
    id:false
  });
  const [dataEditar, setDataEditar] = React.useState(false); // auxiliar para los datos de editar viaje

  useEffect(() => {
    const viajeService = new ViajeService();
    /*** se carga la data de la api */
    viajeService.getViajes().then(data =>{
      setViajes(data) 
    });

  }, []);

  /* Con esta funcion agrupamos la data en paginas */
  const getDataPerPage = () => {
    let dataAux = viajes ? viajes : data;
    if (filter !== "") {
      dataAux = dataAux.filter(
        (row) =>
          JSON.stringify(row.id).includes(filter) ||
          row.codigo.toLowerCase().includes(filter) ||
          row.numero_plazas.toLowerCase().includes(filter) ||
          row.destino.toLowerCase().includes(filter) ||
          row.lugar.toLowerCase().includes(filter) ||
          row.precio.toLowerCase().includes(filter)
      );
    }

    let agroupedData = [];

    do {
      agroupedData = [...agroupedData, [...dataAux.slice(0, 5)]];
      dataAux = dataAux.slice(5, dataAux.length);
    } while (dataAux.length > 0);

    return agroupedData;
  };

  const handleChange = (e) => {
    //setFilter(e.target.value.toLowerCase());
  };
  
  /*** handleChange del formulario drawer agregar viaje */
  const handleChangeAgregar= (event) => {
    setDatos({ ...datos, [event.target.name]: event.target.value });
  };
      /*** handleChange del formulario drawer editar viaje */
  const handleChangeEditar= (event) => {
    setDataEditar({ ...dataEditar, [event.target.name]: event.target.value });
  };

  const addViaje = async () => {
    const viajeService = new ViajeService();
    viajeService.addViaje(datos).then(data =>{
      viajeService.getViajes().then(data =>{setViajes(data)});
      setDrawerAgregar(false);
    }); 
  }

  const eliminarViaje = async () => {
    const viajeService = new ViajeService();
    viajeService.eliminarViaje(datos.id).then(data =>{
      viajeService.getViajes().then(data =>{setViajes(data)});
      setIsOpen(false);
    }); 
  }

  const actualizarViajero = async () => {
    const viajeService = new ViajeService();
    viajeService.updateViaje(dataEditar).then(data =>{
      viajeService.getViajes().then(data =>{setViajes(data)});
      setDrawerEditar(false);
    }); 
  }

  const prevPage = () => {
    if (page > 0) {
      setPage(page - 1);
    }
  };

  const nextPage = () => {
    if (page < getDataPerPage().length - 1) {
      setPage(page + 1);
    }
  };

  /**
   * Funcion para abrir o cerrar el sidenav
   * @param {*} open true para abrir, false para cerrar
   */
  const toggleDrawer = (open) => (event) => {
    if (event.type === "keydown" && (event.key === "Tab" || event.key === "Shift")) {
      return;
    }

    setDrawerAgregar(open);
    setDrawerEditar(open);
  };

    /*** funcion para button de editar viajero */
    const DrawerEditar = (row) => {
      setDataEditar(row) // se guarda los datos del item en una variable auxiliar
      setDrawerEditar(true) // se abre el drawer de editar viajero
    }

  /** abrir el modal  */
  function openModal(id) {
    setIsOpen(true);
    setDatos({ ...datos, id });
  }
  /** cerrar el modal  */
  function closeModal() {
    setIsOpen(false);
  } 

  return (
    <>
      <div className="flex pt-5 pb-8 justify-between gap-20">
        <div className="flex w-full">
          <input
            placeholder="Busqueda de Ofertas"
            onChange={(e) => handleChange(e)}
            className="custom-input mr-10 w-full i-max-w-1"
          />

          <button onClick={() => setDrawerAgregar(true)} className="btn btn-1 i-w-1" variant="contained">
            Agregar
          </button>
        </div>
      </div>

      <table className="table">
        <tr>
          <th className="text-xl">Codigo</th>
          <th className="text-xl">Numero Plazas</th>
          <th className="text-xl">Destino</th>
          <th className="text-xl">Lugar de origen</th>
          <th className="text-xl">Precio</th>
          <th className="text-xl">Acciones</th>
        </tr>
        {getDataPerPage()[page].map((row) => (
          <tr>
            <td className="row-item text-base">{row.codigo}</td>
            <td className="row-item text-base">{row.numero_plazas}</td>
            <td className="row-item text-base">{row.destino}</td>
            <td className="row-item text-base">{row.lugar}</td>
            <td className="row-item text-base">{row.precio} $</td>
            <td>
              <button onClick={() => DrawerEditar(row)}>
                <CreateIcon />
              </button>
              <button onClick={() => openModal(row.id)}>
                <DeleteIcon />
              </button>
            </td>
          </tr>
        ))}
      </table>

      <Paginator page={page} setPage={setPage} prevPage={prevPage} nextPage={nextPage} pageArray={getDataPerPage()} />

      {/*Area de Drawers*/}
      <Drawer anchor="right" open={drawerAgregar} onClose={toggleDrawer(false)}>
        <AgregarViaje addViaje={addViaje} toggleDrawer={toggleDrawer} handleChange={handleChangeAgregar} />
      </Drawer>

      <Drawer anchor="right" open={drawerEditar} onClose={toggleDrawer(false)}>
        <EditarViaje dataEditar={dataEditar} actualizarViajero={actualizarViajero} toggleDrawer={toggleDrawer} handleChange={handleChangeEditar} />
      </Drawer>

      {/*Area de Modales*/}
      <EliminarViaje eliminarViaje={eliminarViaje} modalIsOpen={modalIsOpen} closeModal={closeModal} />
    </>
  );
};

const data = [
  {
    id: 1,
    codigo: "Gas natural S.A.S",
    numero_plazas: "2",
    destino: "3016578474",
    lugar: "Proveedor",
    precio: "2.3",
  }
];
