import React from "react";
import "./Users.scss";

import { Viajero } from "./Viajero/Viajero";
import { Viajes } from "./Viajes/Viajes";

export const Users = () => {
  const [tabValue, setTabValue] = React.useState(0);

  return (
    <div>
      <div className="w-full">
        <ul className="tab-list flex gap-8">
          <li
            onClick={() => setTabValue(0)}
            className={`tab-item${tabValue === 0 && "-active"} cursor-pointer px-12 py-1 text-xl`}
          >
            Viajeros
          </li>
          <li
            onClick={() => setTabValue(1)}
            className={`tab-item${tabValue === 1 && "-active"} px-12 cursor-pointer py-1 text-xl`}
          >
            Viajes
          </li>
        </ul>
        <div className="tab-content">{tabValue === 0 && <Viajero />}</div>
        <div className="tab-content">{tabValue === 1 && <Viajes />}</div>
      </div>
    </div>
  );
};
