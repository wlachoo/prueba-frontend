
export class ViajeService { 
 
  getViajes() {
    return fetch(`http://127.0.0.1:8000/viajes`)
    .then(response => response.json())
    .then(data => data); 
  }  

  updateViaje(data) {
    return fetch(`http://127.0.0.1:8000/viaje/${data.id}`, {
      method: 'PUT',  
      body: JSON.stringify(data), // data can be `string` or {object}!
      headers:{
        'Content-Type': 'application/json'
      }
    }).then(res => res.json())
    .then(response => response );
  } 

  eliminarViaje(id) {
    return fetch(`http://127.0.0.1:8000/viaje/${id}`, {
      method: 'DELETE',  
      //body: JSON.stringify({password,email}), // data can be `string` or {object}!
      headers:{
        'Content-Type': 'application/json'
      }
    }).then(res => res.json())
    .then(response => response );
  } 

  addViaje(data) {

    var formData = new FormData();
    formData.append('nombre', data.nombre); 
    formData.append('cedula', data.cedula);
    formData.append('fecha', data.fecha); 
    formData.append('telefono', data.telefono);
    formData.append('viaje', data.viaje); 
    console.log(data)
    let plazas =parseInt(data.numero_plazas);
    let precio =parseFloat(data.precio);
    console.log(precio)
    return fetch(`http://127.0.0.1:8000/viaje`, {
      method: 'POST',  
      body: JSON.stringify({ codigo: data.codigo, numero_plazas:plazas,destino:data.destino,lugar:data.lugar,precio:precio }), 
    }).then(res => res.json())
    .then(response => response );
  } 


}