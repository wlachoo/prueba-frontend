
export class ViajeroService { 
 
  getViajeros() {
    return fetch(`http://127.0.0.1:8000/viajeros`)
    .then(response => response.json())
    .then(data => data); 
  }  

  updateViajero(data) {
    return fetch(`http://127.0.0.1:8000/viajero/${data.id}`, {
      method: 'PUT',  
      body: JSON.stringify(data), // data can be `string` or {object}!
      headers:{
        'Content-Type': 'application/json'
      }
    }).then(res => res.json())
    .then(response => response );
  } 

  eliminarViajero(id) {
    return fetch(`http://127.0.0.1:8000/viajero/${id}`, {
      method: 'DELETE',  
      //body: JSON.stringify({password,email}), // data can be `string` or {object}!
      headers:{
        'Content-Type': 'application/json'
      }
    }).then(res => res.json())
    .then(response => response );
  } 

  addViajero(data) {

    var formData = new FormData();
    formData.append('nombre', data.nombre); 
    formData.append('cedula', data.cedula);
    formData.append('fecha', data.fecha); 
    formData.append('telefono', data.telefono);
    formData.append('viaje', data.viaje); 
    return fetch(`http://127.0.0.1:8000/viajero`, {
      method: 'POST',  
      body: JSON.stringify({ nombre: data.nombre, cedula:data.cedula,fecha:data.fecha,telefono:data.telefono,viaje:data.viaje }), 
    }).then(res => res.json())
    .then(response => response );
  } 

}