import React from "react";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";

import "./EditarViaje.scss";

export const EditarViaje = ({dataEditar,actualizarViajero, handleChange, toggleDrawer }) => {
  return (
    <div className="filter-form-container p-8 h-full relative flex flex-col">
      <div className="flex items-center gap-4 pb-5">
        <a href="#" onClick={() => toggleDrawer(false)}><ArrowBackIcon /></a>
        <span className="text-2xl font-bold">Editar Viajero</span>
      </div>
        <div>
          {/* <div className="filter-heaader-title">
            <h4>Nuevo viajero</h4>
          </div> */}
          <div className="custom-input-group">
            <label className="custom-label">Codigo</label>
            <input onChange={(e) => handleChange(e)} name={"codigo"} value={dataEditar.codigo} className="custom-input" />
          </div>
          <div className="custom-input-group">
            <label className="custom-label">Numero de Plazas</label>
            <input onChange={(e) => handleChange(e)} type={"number"} name={"numero_plazas"} value={dataEditar.numero_plazas} className="custom-input" />
          </div>
          <div className="custom-input-group">
            <label className="custom-label">Destino</label>
            <input onChange={(e) => handleChange(e)} name={"destino"} value={dataEditar.destino} className="custom-input" />
          </div>
          <div className="custom-input-group">
            <label className="custom-label">Lugar de Origen</label>
            <input onChange={(e) => handleChange(e)} name={"lugar"} value={dataEditar.lugar} className="custom-input" />
          </div>
          <div className="custom-input-group">
            <label className="custom-label">Precio</label>
            <input onChange={(e) => handleChange(e)} type={"number"} name={"precio"} value={dataEditar.precio} className="custom-input" />
          </div>
        </div>
        <div className="flex justify-between px-4 gap-12 editar-button">
          <button onClick={() => toggleDrawer(false)} className="btn btn-3 i-w-full" variant="contained">
            Cancelar
          </button>
          <button onClick={() => actualizarViajero()} className="btn btn-1 i-w-full" variant="contained">
            Agregar
          </button>
        </div>
    </div>
  );
};
