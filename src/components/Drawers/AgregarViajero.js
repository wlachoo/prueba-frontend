import React from "react";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";

import "./AgregarViajero.scss";

export const AgregarViajero = ({ handleChangeAgregar, toggleDrawer,agregarViajero }) => {
  return (
    <div className="filter-form-container p-8 h-full relative flex flex-col">
      <div className="flex items-center gap-4 pb-5">
        <ArrowBackIcon />
        <span className="text-2xl font-bold">Agregar Viajero</span>
      </div>
        <div>
          {/* <div className="filter-heaader-title">
            <h4>Nuevo viajero</h4>
          </div> */}
          <div className="custom-input-group">
            <label className="custom-label">Nombre</label>
            <input onChange={(e) => handleChangeAgregar(e)} name={"nombre"} className="custom-input" />
          </div>
          <div className="custom-input-group">
            <label className="custom-label">Cedula</label>
            <input onChange={(e) => handleChangeAgregar(e)} name={"cedula"} className="custom-input" />
          </div>
          <div className="custom-input-group">
            <label className="custom-label">Telefono</label>
            <input onChange={(e) => handleChangeAgregar(e)} name={"telefono"} className="custom-input" />
          </div>
          <div className="custom-input-group">
            <label className="custom-label">Fecha Nacimiento</label>
            <input onChange={(e) => handleChangeAgregar(e)} name={"fecha"} type={"date"} className="custom-input" />
          </div>
          <div className="custom-input-group">
            <label className="custom-label">viaje</label>
            <input onChange={(e) => handleChangeAgregar(e)} name={"viaje"} className="custom-input" />
          </div>
        </div>
        <div className="flex justify-between px-4 gap-12 editar-buttons">
          <button onClick={() => toggleDrawer(false)} className="btn btn-3 i-w-full" variant="contained">
            Cancelar
          </button>
          <button onClick={() => agregarViajero()} className="btn btn-1 i-w-full" >
            Agregar
          </button>
        </div>
    </div>
  );
};
