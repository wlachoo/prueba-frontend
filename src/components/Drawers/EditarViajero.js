import React from "react";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";

import "./EditarViajero.scss";

export const EditarViajero = ({dataEditar,actualizarViajero, handleChangeAgregar, toggleDrawer }) => {
  return (
    <div className="filter-form-container p-8 h-full relative flex flex-col">
      <div className="flex items-center gap-4 pb-5">
        <ArrowBackIcon />
        <span className="text-2xl font-bold">Editar Viajero</span>
      </div>
        <div>
          {/* <div className="filter-heaader-title">
            <h4>Nuevo viajero</h4>
          </div> */}
          <div className="custom-input-group">
            <label className="custom-label">Nombre</label>
            <input onChange={(e) => handleChangeAgregar(e)} value={dataEditar.nombre} name={"nombre"} className="custom-input" />
          </div>
          <div className="custom-input-group">
            <label className="custom-label">Cedula</label>
            <input onChange={(e) => handleChangeAgregar(e)} value={dataEditar.cedula} name={"cedula"} className="custom-input" />
          </div>
          <div className="custom-input-group">
            <label className="custom-label">Telefono</label>
            <input onChange={(e) => handleChangeAgregar(e)} value={dataEditar.telefono} name={"telefono"} className="custom-input" />
          </div>
          <div className="custom-input-group">
            <label className="custom-label">Fecha Nacimiento</label>
            <input onChange={(e) => handleChangeAgregar(e)} value={dataEditar.fecha} name={"fecha"} className="custom-input" />
          </div>
        </div>
        <div className="flex justify-between px-4 gap-12 editar-button">
          <button onClick={() => toggleDrawer(false)} className="btn btn-3 i-w-full" variant="contained">
            Cancelar
          </button>
          <button onClick={() => actualizarViajero()} className="btn btn-1 i-w-full" variant="contained">
            Agregar
          </button>
        </div>
    </div>
  );
};
