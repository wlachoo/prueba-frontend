import React from "react";
//import image from "./../../../assets/img/users/127.svg";
import { ModalWrapper } from "../ModalWrapper";
//import ArrowForward from "@material-ui/icons/ArrowForward"; 

 
export const EliminarViaje = ({eliminarViaje, modalIsOpen, closeModal }) => {
  return (
    <ModalWrapper modalIsOpen={modalIsOpen} closeModal={closeModal}>
      <div className="w-96 mb-20 flex flex-col items-center ">
        <h2 className="text-1 mb-6">Eliminar Viaje</h2>
        <p className="text-2">¿ desea eliminar este viaje de la lista ?</p>
        <div className="profile-avatar">
            {/* <img className="avatar" src={image} alt=""/> */}
            {/* <img className="avatar-edit" src={EliminarUsuario.image} alt="Preview" /> */}
        </div>
        <div className="flex flex-col items-center nombre-position">
            <span className="font-bold text-xl"></span>
            <span className="text-xl position"></span>
        </div>
      </div>

      <div className="w-full ">
        <button onClick={() => closeModal()} className="btn btn-3 w-auto float-left px-8 boton-cancelar">
          Cancelar
        </button>
        <button onClick={() => eliminarViaje()} className="btn btn-1 w-auto float-right px-8 boton-eliminar">
          Eliminar
        </button>

      </div>

    </ModalWrapper>
  );
};
