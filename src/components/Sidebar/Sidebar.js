import React from "react";
import "./Sidebar.scss";
import { NavLink } from "react-router-dom";

import active_users from "../../assets/img/common/active_users.svg";
import inactive_users from "../../assets/img/common/inactive_users.svg";

export const Sidebar = () => {
  return ( 
    <div className="sidebar flex flex-col pt-8">
      <ul className="mt-7 flex flex-col">
        <li>
          <NavLink activeClassName="active-link" className="px-4 py-7 flex items-center" to="users">
            <img className="active-icon w-9 mr-6 " src={active_users} alt="" />
            <img className="inactive-icon w-9 mr-6 " src={inactive_users} alt="" />
            <span>Viajeros</span>
          </NavLink>
        </li>
      </ul>
    </div>
  );
};
