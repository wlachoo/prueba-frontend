import React from "react";
import "./Main.scss";
import { Switch, Route, Redirect } from "react-router-dom";
import { Users } from "../../pages/Users/Users";

export const Main = () => {
  return (
    <div className="main w-full h-full p-12">
      <Switch>
        <Route path="/users" component={Users} />
        <Redirect from="/" to="/users" />
      </Switch>
    </div>
  );
};
